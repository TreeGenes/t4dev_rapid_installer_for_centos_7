Requirements
============
* At least 1024MB RAM
* Clean CENTOS 7 64-bit installed

Tips
============
* You can use Oracle VirtualBox to run a virtual machine on your own system (https://www.virtualbox.org/)
* You can then install CENTOS 7 or even download an already installed version of CENTOS 7 via http://www.osboxes.org

WARNING: We cannot vouch for the osboxes.org virtual disk files but we used it during testing and it works.

Installation
============

MAJOR ASSUMPTION: YOU MUST HAVE A CLEAN CENTOS7 INSTALLED.
THIS SCRIPT USES DEFAULT FILES WHICH MAY OVERWRITE YOUR CUSTOMIZED FILES - DO NOT USE ON AN ALREADY USED CENTOS 7!

This will set up and install all necessary requirements for Tripal. Particularly:
* PHP 7.2 from REMI as well as additional PHP modules needed for Tripal and Drupal
* Drush 10.2.x
* Miscellaneous configuration files required to run Postgres, Drupal and Tripal


Run the following commands in bash / command line as root:

yum install -y git

mkdir /t4dev

cd /t4dev

git clone https://gitlab.com/TreeGenes/t4dev_rapid_installer_for_centos_7.git

cd t4dev_rapid_installer_for_centos_7

chmod 0755 t4dev_rapid_installer_centos7.sh

./t4dev_rapid_installer_centos7.sh


