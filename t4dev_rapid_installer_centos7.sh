#!/bin/bash
# Author: Risharde Ramnath (www.risharde.com)
# Written and tested on 11th April 2020
# Support Tripal by visiting http://tripal.info/
#
#
echo "Welcome to Tripal4 Rapid Installer for CENTOS 7"
echo "-----------------------------------------------"
echo ""
echo "* Make sure server has at least 2GB of RAM!"
echo ""
echo "[INFO] Setting up DRUPAL_HOME variable ..."
DRUPAL_HOME=/var/www/html
echo "[INFO] Installing CENTOS Development Tools..."
echo "[INFO] You need to be connected to the internet, this takes a while..."
yum groupinstall "Development Tools" -y
echo "[INFO] Installing WGET and NANO ..."
yum install wget nano -y
echo "[INFO] Installing Apache HTTPD Server ..."
yum install httpd -y
echo "[INFO] Setting up HTTPD service settings ..."
systemctl start httpd.service
systemctl enable httpd.service
systemctl disable firewalld
systemctl stop firewalld
echo "[INFO] Copying httpd.conf to HTTPD settings folder ..."
yes | cp -rf httpd.conf /etc/httpd/conf/httpd.conf
echo "[INFO] Restarting HTTPD service ..."
systemctl restart httpd.service
echo "[INFO] Install EPEL Release"
yum install epel-release -y
echo "[INFO] Install REMI REPO to get PHP 7.3 packages"
yum install http://rpms.remirepo.net/enterprise/remi-release-7.rpm -y
yum install yum-utils -y
yum-config-manager --enable remi-php73
yum install php php-mbstring php-mcrypt php-cli php-gd php-curl php-xml php-mysql php-pgsql php-ldap php-zip php-fileinfo -y
php -v
yes | cp -rf php.ini /etc/php.ini
systemctl restart httpd.service
# Install the repository RPM:
sudo yum install -y https://download.postgresql.org/pub/repos/yum/reporpms/EL-7-x86_64/pgdg-redhat-repo-latest.noarch.rpm

# Install PostgreSQL:
yum remove postgresql-server -y
yum remove -y postgresql10-server
yum install -y postgresql10-server

# Optionally initialize the database and enable automatic start:
/usr/pgsql-10/bin/postgresql-10-setup initdb
systemctl enable postgresql-10
systemctl start postgresql-10

#yum remove postgresql-server -y
#yum install postgresql-server -y
#postgresql-setup initdb


yes | cp -rf pg_hba.conf /var/lib/pgsql/10/data/pg_hba.conf
systemctl enable postgresql-10
chcon -R -t httpd_sys_content_rw_t /var/www/html
setsebool -P httpd_can_network_connect_db on
#wget https://github.com/drush-ops/drush/releases/download/8.3.0/drush.phar
#php drush.phar core-status
#chmod +x drush.phar
#yes | mv drush.phar /usr/bin/drush


#drush init
#echo "If you are asked for a role password, use 'drupal' without the quotes"
#echo "Later on you'll need this to set up the database in which case you will use 'drupal' for all the values asked"
#su postgres -c 'createuser -P drupal'
#su postgres -c 'createdb drupal -O drupal'
DRUPAL_HOME=/var/www/html
cd $DRUPAL_HOME



### THIS WAS FOR RAPID INSTALLER FOR TRIPAL 3 STABLE
#git clone https://github.com/tripal/tripal_install.git
#mv tripal_install/* ./
#drush --include=. tripal-generic-install

### THIS SHOULD BE SET UP TO DEAL WITH THE T4DEV FILES

### INSTALL COMPOSER
rm -rf installer*
rm -rf /usr/loca/bin/composer
wget https://getcomposer.org/installer
php installer
mv composer.phar /usr/local/bin/composer

cd $DRUPAL_HOME

### IMPROVISE TO GET DRUSH - Install it right within /var/www/html
rm -rf /usr/bin/drush
composer require drush/drush
ln -s /var/www/html/vendor/bin/drush /usr/bin/drush

### This is where composer and Drupal 8 simply is not ready for production - it runs out again of memory at 2GB!!! Unbelievable!
# composer require drush/drush
su postgres -c "psql --command='DROP DATABASE tripal4dev'"
su postgres -c "psql --command='DROP USER tripal4dev'"
su postgres -c "psql --command=\"CREATE USER tripal4dev WITH PASSWORD 'tripal4dev'\""
su postgres -c "psql --command=\"CREATE DATABASE tripal4dev WITH OWNER tripal4dev\""



export COMPOSER_MEMORY_LIMIT=-1
### The below command was deprecated because of memory issues and could not be automated
#composer create-project drupal-composer/drupal-project:8.x-dev tripal4 --stability dev --no-interaction
### The below command is the new command which does not spit out ugly memory errors
#composer create-project drupal/core tripal4 --stability dev --no-interaction #DOES NOT WORK
#composer create-project drupal/core tripal4 --no-interaction
rm -rf $DRUPAL_HOME/tripal4
composer create-project drupal/recommended-project tripal4 --no-interaction
cd $DRUPAL_HOME/tripal4/web/sites/default
mkdir $DRUPAL_HOME/tripal4/web/sites/default/files
chmod 0777 $DRUPAL_HOME/tripal4/web/sites/default/files
cp $DRUPAL_HOME/tripal4/web/sites/default/default.settings.php $DRUPAL_HOME/tripal4/web/sites/default/settings.php
chmod 0777 $DRUPAL_HOME/tripal4/web/sites/default/settings.php


echo ""
echo ""
echo "----------------------------------------------------------------------------------------"
echo "The files are installed, you need to continue this part manually due to memory issues"
echo "Go to your http://<ip address>/tripal4/web and enter the following during installation:"
echo "Select database type as POSTGRESQL"
echo "DB NAME: tripal4dev"
echo "DB USERNAME: tripal4dev"
echo "DB PASSWORD: tripal4dev"
echo ""
echo "When you have finished the basic site setup, press enter and this script will install tripal4 to the site..."
read -p "Press enter to continue"
cd $DRUPAL_HOME/tripal4

DRUPAL_HOME=/var/www/html
cd $DRUPAL_HOME
cd $DRUPAL_HOME/tripal4
cd $DRUPAL_HOME/tripal4/web/modules
git clone https://github.com/tripal/t4d8.git
$DRUPAL_HOME/vendor/bin/drush en tripal
drush cache-rebuild
echo "Done, the Tripal4 modules should be installed on your site!"


#$DRUPAL_HOME/vendor/bin/drush site-install standard \
#  --db-url=pgsql://tripal4dev:tripal4dev@localhost/tripal4dev \
#  --account-mail="tripaladmin@localhost" \
#  --account-name=admin \
#  --account-pass=admin \
#  --site-mail="admin@localhost" \
#  --site-name="Tripal 4 Development"
  

systemctl restart httpd.service


